### Ansible module for realtime putput from shell

```
- hosts: linux
  gather_facts: no
  tasks:
    - name: test script
      slow_command:
        cmd: for i in $(seq 1 10); do echo line$i; sleep 1; done
```

#### Change tunnel ports in ssh

1. Define port in task
```
- name: test script
  environment:
    SLOW_CMD_PORT: 9000
  slow_command:
    cmd: for i in $(seq 1 10); do echo line$i; sleep 1; done
```

2. Change ssh ports in hosts:
```
[linux:vars]
ansible_ssh_extra_args='-R9000:localhost:9000'
```

3. Define variable on ansible control host
```
SLOW_CMD_PORT=9000 ansible-playbook -v rt_shell_play.yml
```