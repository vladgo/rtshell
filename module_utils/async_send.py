import socket
import os

SEND_HOST = '127.0.0.1'
SEND_PORT = int(os.getenv('SLOW_CMD_PORT', '9999'))

def send_msg(msg):
    for line in msg.split("\n"):
        if line:
            sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            sock.connect((SEND_HOST, SEND_PORT))
            sock.send("<{0}> {1}\n".format(socket.gethostname(), line))
            sock.close()

